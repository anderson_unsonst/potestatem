<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


  Route::get('/', 'PrincipalController@index');
  Route::get('alunos/relatorio', 'AlunosController@relatorio');

  Route::resource('cursos', 'CursosController');
  Route::resource('alunos', 'AlunosController');
  Route::resource('alunocertificados', 'AlunoCertificadosController');
  Route::resource('principal', 'PrincipalController');


