<script src="https://code.jquery.com/jquery-3.3.1.min.js"
              integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="container">
<h1>Adicionar novo produto</h1>
<form class="" action="/alunos" method="POST">
    <div class="form-group">
	<label>Nome do Aluno: </label>
    <input type="text" name="nome" value="" placeholder="Nome" class="form-control">
    {{ ($errors->has('nome')) ? $errors->first('nome') : '' }}
<div class="checkbox">
    <label> Matriculado</label>: <input type="checkbox" name="matriculado" value="1" checked placeholder="Matriculado">
    {{ ($errors->has('matriculado')) ? $errors->first('matriculado') : '' }}
</div>
</div>
<div class="form-group">
    <label>Telefone: </label>
    <input type="text" name="telefone" value="" placeholder="Telefone" class="form-control">
    {{ ($errors->has('telefone')) ? $errors->first('telefone') : '' }}<br>
</div>
<div class="form-group">
    <label>E-mail</label>
    <input type="email" name="email" value="" placeholder="Email" class="form-control">
    {{ ($errors->has('email')) ? $errors->first('email') : '' }}<br>
</div>
<div class="form-group">
    <label>Data de Nascimento</label>
    <input type="date" name="data_nascimento"  placeholder="data_nascimento" class="form-control">
    {{ ($errors->has('data_nascimento')) ? $errors->first('data_nascimento') : '' }}<br>
</div>
<div class="form-group">
    <label>Senha</label>
    <input type="password" name="senha" value="" placeholder="Senha" class="form-control">
    {{ ($errors->has('senha')) ? $errors->first('senha') : '' }}<br>
</div>
<div class="form-group">
    <label>Data do Cadastro</label>
    <input type="text" readonly="readonly" name="datacadastro" value="{{date('d/m/Y')}}" placeholder="Data do Cadastro" class="form-control">
    {{ ($errors->has('datacadastro')) ? $errors->first('datacadastro') : '' }}<br>
</div>
<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit" name="name" value="Salvar">
</div>
</form>
</div>