<script src="https://code.jquery.com/jquery-3.3.1.min.js"
              integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<script type="text/javascript">
 $('document').ready( function() {
  
    $('.detalhes').on('click', function(e){
         e.preventDefault();
        $.ajax({ 
          type: "GET", 
          url: $(this).attr('data-link'),
          dataType: 'json',
          success: function(data){ 
             
            htmlData = '<ul><li>Nota: '+data.nota+'</li><li>Data de conclusão: '+data.dataconclusao+'</li></ul>';
            $('#myModal').find('.modal-body').html(htmlData);
         
          }
        });

    });

});
</script>



<div class="container">
    {{ Session::get('message') }}
<div class="col-sm-3 col-md-3 pull-left">
<h1>Alunos</h1>

 
<a href="/alunos/create" class="btn btn-primary" >Cadastrar novo</a><br>


</div>

     <div class="col-sm-3 col-md-3 pull-right">
<form action="/alunos/search"  >
    <div class="input-group">
              <input type="text" class="form-control" placeholder="Busca" name="busca" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
          </div>
</form>
</div>
   
  <table class="table table-striped">
    <thead>
        <th>#</th>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Telefone</th>
    </thead>
    @foreach($Alunos as $Aluno)
      
      <tr>
        <td><a href="/alunos/{{ $Aluno->id }}">{{ $Aluno->id }}</a></td>
        <td><a href="/alunos/{{ $Aluno->id }}">{{ $Aluno->nome }}</a></td>
        <td><a href="/alunos/{{ $Aluno->id }}">{{ $Aluno->email }}</a></td>
        <td><a href="/alunos/{{ $Aluno->id }}">{{ $Aluno->telefone }}</a></td>
        <td><button type="button" class="detalhes btn btn-primary" data-link="/alunocertificados/{{ $Aluno->id }}" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> Ver Detalhes</button> </td>
      </tr>
       
        <hr>
    @endforeach
</table>
{{$Alunos->render()}}
<a href="/principal" class="btn btn-primary">Voltar</a>

</div> <!-- container-->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detalhes</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fecha</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->