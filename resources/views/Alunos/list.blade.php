<script src="https://code.jquery.com/jquery-3.3.1.min.js"
              integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div class="container">
<h1>Relatórios</h1>

<table class="table table-striped">
    <thead>
    <th>Nome </th>
    <th> Certificados</th>
</thead>
<tbody>
    @foreach($Alunos as $aluno)
    <tr>
        <td>{{$aluno->nome}} </td>
        <td>{{$aluno->certificado}}</td>
    </tr>
    @endforeach
</tbody>
</table>

<a href="/principal" class="btn btn-primary">Voltar</a>
</div>