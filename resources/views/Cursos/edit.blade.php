<h1>Editar produto</h1>
<form action="/cursos/{{ $cursos->id }}" method="POST">
    <input type="text" name="nome" value="{{ $cursos->nome }}" placeholder="Nome">
    {{ ($errors->has('nome')) ? $errors->first('nome') : '' }}<br>
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit" name="name" value="Salvar">
</form>

