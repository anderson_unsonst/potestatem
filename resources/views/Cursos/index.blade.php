<script src="https://code.jquery.com/jquery-3.3.1.min.js"
              integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


{{ Session::get('message') }}
<div class="container" >
<h1>Cursos</h1>
<a href="/cursos/create">Cadastrar novo</a>
    @foreach($Cursos as $curso)
        <h2><a href="/cursos/{{ $curso->id }}">{{ $curso->nome }}</a></h2>
     
        <a href="/cursos/{{ $curso->id }}/edit" class="btn">Editar</a>
        <form action="/cursos/{{ $curso->id }}" method="POST">
            <input type="hidden" name="_method" value="delete">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" name="name" value="Apagar" class="btn">
        </form>
        <hr>
    @endforeach
<a href="/principal" class="btn btn-primary">Voltar</a>    
</div>