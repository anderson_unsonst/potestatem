<h1>Adicionar novo curso</h1>
<form class="" action="/cursos" method="POST">
	<label>Nome do Alunos: </label>
    <input type="text" name="nome" value="" placeholder="Nome">
    {{ ($errors->has('nome')) ? $errors->first('nome') : '' }}<br>
    Alunos Ativo: <input type="checkbox" name="inativo" value="1" checked placeholder="Inativo">
    {{ ($errors->has('inativo')) ? $errors->first('inativo') : '' }}<br>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit" name="name" value="Salvar">
</form>