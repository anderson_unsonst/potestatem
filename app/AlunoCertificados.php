<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * classe AlunoCertificados
 */
class AlunoCertificados extends Model {

    protected $table = 'aluno_certificado';

}
