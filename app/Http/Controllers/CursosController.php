<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cursos;
/*
 * class Cursos 
 */
class CursosController extends Controller {

    /*
     * metodo index
     * 
     * @param Request $request 
     * 
     * @return view
     */
    public function index(Request $request) {

        $cursos = Cursos::orderBy('created_at', 'desc')->paginate(10);
        return view('Cursos.index', ['Cursos' => $cursos]);
    }

    /*
     * metodo create
     * 
     * @return view
     */
    public function create() {
        return view('Cursos.create');
    }

    /*
     * metodo store
     * 
     * @param Request $request
     * 
     * @return bool
     */
    public function store(Request $request) {
        $cursos = new cursos;
        $cursos->nome = $request->nome;

        if ($request->inativo === 1) {
            $cursos->inativo = 1;
        } else {
            $cursos->inativo = 0;
        }

        $cursos->save();
        return redirect('cursos')->with('message', 'cursos criado com sucesso!');
    }

    /*
     * metodo edit
     * 
     * @param int $id
     * 
     * @return view
     */
    public function edit($id) {
        $cursos = Cursos::findOrFail($id);
        return view('Cursos.edit', compact('cursos'));
    }

    /*
     * metodo show
     * 
     * @param int $id
     * 
     * @return view
     */
    public function show($id) {
        $cursos = Cursos::find($id);
        if (!$cursos) {
            abort(404);
        }
        return view('Cursos.details')->with('Cursos', $cursos);
    }

    /*
     * metodo update
     * 
     * @param Request $request
     * @param int $id 
     * 
     * @return void
     */
    public function update(Request $request, $id) {
        $cursos = Cursos::findOrFail($id);
        $cursos->nome = $request->nome;

        $cursos->save();
        return redirect('cursos')->with('message', 'cursos atualizado com sucesso!');
    }

    /*
     * metodo destroy
     * @param int $id
     * 
     * @return void
     */
    public function destroy($id) {
        $cursos = Cursos::findOrFail($id);
        $cursos->delete();
        return redirect('cursos')->with('alert-success', 'cursos foi deletedo!');
    }

}
