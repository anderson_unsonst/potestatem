<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\AlunoCertificados;

/*
 * classe AlunoCertificadosController
 */

class AlunoCertificadosController extends Controller {
    /*
     * metodo index
     */

    public function index() {
        
    }

    /*
     * metodo show
     * 
     * @param int $id 
     * 
     */

    public function show($id) {
        $certificados = AlunoCertificados::findOrFail($id);
        $certificados->dataconclusao = date('d/m/Y', strtotime($certificados->dataconclusao));
        return json_encode($certificados);
    }

}
