<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Alunos;

/*
 * classe AlunosController
 */

class AlunosController extends Controller {
    /*
     * metodo index
     * 
     * @param Request $request 
     * 
     * @return View
     */

    public function index(Request $request) {

        $busca = $request->get('busca');
        $Alunos = Alunos::orderBy('created_at', 'desc')->paginate(1);
        if (!empty($busca)) {
            $Alunos = Alunos::where('nome', 'LIKE', '%' . $busca . '%');
            $Alunos->appends(['search' => $busca]);
        }
        return view('Alunos.index', ['Alunos' => $Alunos]);
    }

    /*
     * metodo create
     * 
     * @return view
     */

    public function create() {
        return view('Alunos.create');
    }

    /*
     * metodo store
     * 
     * @param Request $request
     * 
     * @return bool
     */

    public function store(Request $request) {
        $Alunos = new Alunos;
        $Alunos->nome = $request->nome;

        $Alunos->datacadastro = date('Y/m/d');
        $Alunos->email = $request->email;
        $Alunos->senha = $request->senha;
        $Alunos->telefone = $request->telefone;
        $Alunos->data_nascimento = $request->data_nascimento;

        if ($request->matriculado === 1) {
            $Alunos->matriculado = 1;
        } else {
            $Alunos->matriculado = 0;
        }

        $Alunos->save();
        return redirect('alunos')->with('message', 'Alunos criado com sucesso!');
    }

    /*
     * metodo edit
     * 
     * @param int $id 
     * 
     * @return view
     */

    public function edit($id) {
        $Alunos = Alunos::findOrFail($id);
        return view('Alunos.edit', compact('Alunos'));
    }

    /*
     * metodo show
     * 
     * @param int $id 
     * 
     * @return view  
     */

    public function show($id) {
        $Alunos = Alunos::find($id);
        if (!$Alunos) {
            abort(404);
        }
        return view('Alunos.details')->with('Alunos', $Alunos);
    }

    /*
     * metodo update
     * 
     * @param Request $request
     * @param int $id 
     * 
     * @return bool
     */

    public function update(Request $request, $id) {
        $Alunos = Alunos::findOrFail($id);
        $Alunos->nome = $request->nome;

        $Alunos->save();
        return redirect('alunos')->with('message', 'Alunos atualizado com sucesso!');
    }

    /*
     * metodo destroy
     * 
     * @param int $id 
     * 
     * @return bool
     */

    public function destroy($id) {
        $Alunos = Alunos::findOrFail($id);
        $Alunos->delete();
        return redirect('alunos')->with('alert-success', 'Alunos foi deletedo!');
    }

    /*
     * metodo relatorio
     * 
     * @return Object
     */

    public function relatorio() {
//        $Alunos = $users = DB::table('aluno_certificado')
//                ->join('aluno', 'aluno_certificado.aluno_id', '=', 'aluno.id')
//                ->orderBy('aluno_certificado.aluno_id')
//                ->limit(100)
//               
//                ->get();
       $Alunos =  DB::table('aluno_certificado')
                     ->select(DB::raw('count(*) as certificado, aluno.nome as nome'))
                     ->join('aluno', 'aluno_certificado.aluno_id', '=', 'aluno.id')
                     
                     ->groupBy('aluno.nome')
                     ->get();
             
      
         return view('Alunos.list')->with('Alunos', $Alunos);
    }

}
