<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/*
 * classe Principal
 */
class PrincipalController extends Controller {

    public function index() {

        return view('Principal.index');
    }

}
