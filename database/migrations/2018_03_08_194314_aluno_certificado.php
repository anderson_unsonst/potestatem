<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlunoCertificado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

           Schema::create('aluno_certificado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('aluno_id');
            $table->integer('curso_id');
            $table->date('datamatricula');
            $table->date('dataconclusao');
            $table->double('nota', 10, 2);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aluno_certificado');
    }
}
