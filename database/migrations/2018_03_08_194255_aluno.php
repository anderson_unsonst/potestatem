<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Aluno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

          Schema::create('aluno', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('matriculado');
            $table->date('datacadastro');
            $table->string('nome', 100);
            $table->string('email', 100);
            $table->string('senha', 20);
            $table->string('telefone', 25);
            $table->date('data_nascimento');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aluno');
    }
}
